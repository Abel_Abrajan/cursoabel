package com.disney.interfaces;

public interface Baker {
	
	void makeBread();
}
