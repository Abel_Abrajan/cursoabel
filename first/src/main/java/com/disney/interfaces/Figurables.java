package com.disney.interfaces;

public interface Figurables {
	void calculateArea();
    void calculatePerimeter();
}
