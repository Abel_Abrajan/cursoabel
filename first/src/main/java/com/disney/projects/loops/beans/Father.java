package com.disney.projects.loops.beans;

import com.disney.interfaces.Baker;

public class Father implements Baker {
	
	private String name;
	private int age;
	
	
	public Father() {
		
	}
	
	public Father(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Father [name=" + name + ", age=" + age + "]";
	}

	public void makeBread() {
		System.out.println("You are baking an excellent bread!");
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
