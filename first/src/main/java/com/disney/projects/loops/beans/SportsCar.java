package com.disney.projects.loops.beans;

public class SportsCar extends Vehicle {

	public SportsCar(String brand, String model, String color, String type, int year) {
		super(brand, model, color, type, year);
	}
	
	@Override
	public void makeCar( ) {
		System.out.println("Italian Sports Car");
	}
}
