package com.disney.projects.loops.beans;

public class ConventionalCar extends Vehicle {

	public ConventionalCar(String brand, String model, String color, String type, int year) {
		super(brand, model, color, type, year);
	}
	
	@Override
	public void makeCar( ) {
		System.out.println("Conventional American Car");
	}
}
