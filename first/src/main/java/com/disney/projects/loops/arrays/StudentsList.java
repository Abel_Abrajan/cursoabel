package com.disney.projects.loops.arrays;

import java.util.Scanner;

public class StudentsList {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
        ArraysOperations arraysOperations = new ArraysOperations();
        
        System.out.println("How many students are there in the class?");
        
        int TotalStudents = scanner.nextInt();
        
        String [] arrayStudents = new String[TotalStudents];
        
        int count = 0;
                
        while(count < TotalStudents) {
            System.out.println("What is his name?");
            String studentName = scanner.next();
            arrayStudents[count] =  studentName;
            count++;
        }
        System.out.println(":::: Students names :::: ");
        arraysOperations.showAllArrayEasy(arrayStudents);
	}
	
}
