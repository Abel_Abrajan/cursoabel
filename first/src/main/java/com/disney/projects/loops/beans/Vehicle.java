package com.disney.projects.loops.beans;

import com.disney.interfaces.Car;

public class Vehicle implements Car {

	private String brand;
	private String model;
	private String color;
	private String type;
	private int year;
	
	public Vehicle() {
		
	}
	
	public Vehicle(String brand, String model, String color, String type, int year) {
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.type = type;
		this.year = year;
	}
	
	@Override
	public String toString() {
		return "Vehicle [brand=" + brand + ", model=" + model + ", color=" + color + ", type=" + type + ", year=" + year + "]";
	}

	public void makeCar() {
		System.out.println("Building a great car!");
	}
	
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}
