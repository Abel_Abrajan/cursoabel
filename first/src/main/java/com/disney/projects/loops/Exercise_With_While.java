package com.disney.projects.loops;

import java.util.Scanner;

public class Exercise_With_While {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Perform sum of numbers? (S / N)");
		String answer = scanner.next();
		
		int result = 0;
		
		while (answer.equalsIgnoreCase("S")) {
			System.out.print("Enter number: ");
			int num = scanner.nextInt();
			
			result = result + num;
			
			System.out.println("Keep adding? (S / N): ");
			answer = scanner.next();
		}
		System.out.println("\nResult: " + result);
	}
}
