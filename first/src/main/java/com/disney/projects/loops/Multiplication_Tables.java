package com.disney.projects.loops;

import java.util.Scanner;

public class Multiplication_Tables {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the number");
		int number = scanner.nextInt();
		
		System.out.println("Enter the limit number");
		int limit_number = scanner.nextInt();
		
		System.out.println("\n:::::::::| R E S U L T |:::::::::");
		for (int i = 1; i <= limit_number; i++) {
			System.out.println(number + " X " + i + " = " + (number * i));
		}
	}
}
