package com.disney.projects.loops.beans;

public class SecondSon extends Father {
	
	public SecondSon(String name, int age) {
		super(name, age);
	}

	@Override
	public void makeBread() {
		System.out.println("My father do...");
		System.out.println("Make excellent bread by the second son!");
	}

	@Override
	public String toString() {
		return "SecondSon [name=" + getName() + ", age=" + getAge() + "]";
	}
}
