package com.disney.projects.loops.arrays;

public class ArraysOperations {
	
	public void showAllArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("ID: " + i + " value: " + array[i]);
        }
    }

    public void showAllArrayEasy(int[] array) {
        for (int i : array) {
            System.out.println("Value: " + i);
        }
    }
    
    public void showAllArrayEasy(String [] array) {
        for (String name : array) {
            System.out.println("Name: " + name);
        }
    }
    
}
