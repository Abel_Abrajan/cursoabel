package com.disney.projects.loops;

import java.util.Scanner;

public class Exercise_With_For {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Init Fibonacci: ");
		int init = scanner.nextInt();
		
		System.out.print("Finish Fibonacci: ");
		int finish = scanner.nextInt();
		
		for (int i = init; i <= finish; i++) {
			System.out.println("The Fibonacci ("+ i +") is: " + fib(i));
		}
	}
	
	public static int fib(int n) {
		if (n < 2) {
			return n;
		} else {
			return fib(n - 1) + fib(n - 2);
		}
	}
}
