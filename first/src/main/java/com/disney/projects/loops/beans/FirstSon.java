package com.disney.projects.loops.beans;

public class FirstSon extends Father {
	
	public FirstSon(String name, int age) {
		super(name, age);
	}

	@Override
	public void makeBread() {
		System.out.println("My father do...");
		System.out.println("Make bad bread by the first son!");
	}

	@Override
	public String toString() {
		return "FirstSon [name=" + getName() + ", age=" + getAge() + "]";
	}
}
