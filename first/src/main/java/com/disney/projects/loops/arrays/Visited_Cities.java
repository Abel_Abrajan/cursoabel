package com.disney.projects.loops.arrays;

public class Visited_Cities {

	public static void main(String[] args) {
		System.out.println("::::::: C I T I E S :::::::\n");
		
		String[] city = new String[10];
		
		city[0] = "Tokio";
		city[1] = "Nueva York";
		city[2] = "Los Ángeles";
		city[3] = "Seúl";
		city[4] = "Londres";
		city[5] = "París";
		city[6] = "Osaka";
		city[7] = "Shanghái";
		city[8] = "Chicago";
		city[9] = "Moscú";
		
		for (String cities : city) {
			System.out.println(cities);
		}
	}
}
