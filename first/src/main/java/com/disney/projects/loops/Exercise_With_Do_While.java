package com.disney.projects.loops;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Exercise_With_Do_While {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Random random = new Random();
		int number = random.nextInt(100);
		
		/*
			HACK!, the comment to know the answer
		*/
		
		// System.out.println("The secret number is: " + number);

		String answer = null;
		int enteredNumber;
		
		System.out.println(":::_ WELCOME TO THE GAME! ::: \n");
		
		do {
			System.out.print("\nEnter a number: ");
			enteredNumber = Integer.parseInt(br.readLine());

			if (enteredNumber == number) {
				System.out.println(":::: Congratulations, you're successful! ::::");
			} else {
				if (enteredNumber > number) {
					System.out.println("The number is less(-)");
				} else {
					System.out.println("The number is greater(+)");
				}
				System.out.print("\nContinue? (s / n): ");
				answer = br.readLine();
			}
		}
		while (answer.equals("s") && enteredNumber != number);
	}

}
