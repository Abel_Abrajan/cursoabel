package com.disney.projects;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // Invoke Constructor
        App appInstance = new App();
        
        appInstance.sayHello(); // Imprimiendo el mensaje del metodo
        
        System.out.println(getHello()); // Imprimiendo todo el contenido del metodo con un syso (Syso y Return)
        
        String contenidoGetHello = getHello(); // Metiendo el contenido del metodo en una variable e imprimiendolo
        System.out.println("El contenido de la variable en el return: " + contenidoGetHello); // Imprimiendo la variable
        
        getHello(); // Imprimiendo el contenido del metodo de forma simple sin el return
    }
    
    public void sayHello() {
    	System.out.println("Hello pinche putit@!");
    }
    
    public static String getHello() {
    	System.out.println("You are in getHello Method!");
    	
    	String message = "Hello again putit@! ;) ";
    	return message;
    }
}