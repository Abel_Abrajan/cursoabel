package com.disney.projects;

import java.util.Scanner;

public class ScannerOperations {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please provide the first number: ");
		int a = scanner.nextInt();
		
		System.out.println("Please provide the first number: ");
		int b = scanner.nextInt();
		
		// Results
		multiplication(a, b);
		division(a, b);
		addition(a, b);
		subtraction(a, b);
		modulus(a, b);
		pow(a, b);
		squareRoot(a, b);
	}
	
	public static void multiplication(int a, int b) {
		System.out.println("The result of the multiplication is: " + (a * b));
	}
	
	public static void division(int a, int b) {
		System.out.println("The result of the division is: " + (a / b));
	}
	
	public static void addition(int a, int b) {
		System.out.println("The result of the addition is: " + (a + b));
	}
	
	public static void subtraction(int a, int b) {
		System.out.println("The result of the subtraction is: " + (a - b));
	}
	
	public static void modulus(int a, int b) {
		System.out.println("The result of modulus is: " + (a % b));
	}
	
	public static void pow(int a, int b) {
		System.out.println("The result of the pow is: " + (Math.pow(a, b)));
	}
	
	public static void squareRoot(int a, int b) {
		System.out.println("The result of the square root - A - is: " + Math.sqrt(a));
		System.out.println("The result of the square root - B - is: " + Math.sqrt(b));
	}
}
