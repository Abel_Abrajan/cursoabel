package com.disney.projects;

import java.util.Scanner;

public class SecondGradeEquation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the value of A: ");
		double a = scanner.nextInt();
		
		System.out.println("Enter the value of B: ");
		double b = scanner.nextInt();
		
		System.out.println("Enter the value of C: ");
		double c = scanner.nextInt();
		
		// Results
		algorithm(a, b, c);
	}
	
	public static void algorithm(double a, double b, double c) {
		double discriminant = Math.pow(b, 2) - (4 * a * c);
		
		/*
		System.out.println("\nValue of the discriminant: " + discriminant); // HelpYourself
		*/
		
		if(discriminant < 0) {
			System.out.println("\n:::: The equation has no solution ::::");
		} 
		
		else if (discriminant == 0) {
			System.out.println("\n:::: The equation has only one solution :::::");
			
			double x = (-b) / (2 * a);
			
			System.out.println("The solution of X is: " + x);
		}
		
		else if (discriminant > 0) {
			System.out.println("\n:::: The equation has two solutions ::::");
			
			double x1 =  (-b + Math.sqrt(discriminant)) / (2 * a);
			double x2 =  (-b - Math.sqrt(discriminant)) / (2 * a);
			
			System.out.println("The solution of X1 is: " + x1 );
			System.out.println("The solution of X2 is: " + x2 );
		}
	}

}
