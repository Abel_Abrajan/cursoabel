package com.disney.projects;

import java.util.Scanner;

public class Exercise1_If {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		String user = "Abel";
		String password = "pass123";
		
		System.out.println(":::: LOGIN ::::\n");
		
		System.out.print("Enter User: ");
		String userInput = scanner.nextLine();
		
		System.out.print("Enter Password: ");
		String passwordInput = scanner.nextLine();
		
		if (userInput.equals(user) && passwordInput.equals(password)) {
			System.out.println("Access Granted");
		} else {
			System.out.println("Access Denied");
		}
	}

}
