package com.disney.projects;

import java.util.Scanner;

public class MenuOperationsWith_Switch_2 {

	public static void main(String[] args) {
		MenuOperationsWith_Switch_2 menuOperationsSwitch2 = new MenuOperationsWith_Switch_2();
		menuOperationsSwitch2.menuOperations();
	}
	
	public void menuOperations() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println(":::: OPERATIONS ::::\n");
		
		System.out.println("- - - |Select an Option| - - -");
		System.out.println("1. Multiplication \n2. Division \n3. Addition \n4. Subtraction \n5. Modulus \n6. Pow \n7. Square Root");
		
		System.out.print("\nChoose an option: ");
		int option = scanner.nextInt();
		
		System.out.println("\nFirst number: ");
		int first_number = scanner.nextInt();
		
		System.out.println("Second number: ");
		int second_number = scanner.nextInt();
		
		switch (option) {
		case 1:
			System.out.println(":: Multiplication ::");
			ScannerOperations.multiplication(first_number, second_number);
			break;
		case 2:
			System.out.println(":: Division ::");
			ScannerOperations.division(first_number, second_number);
			break;
		case 3:
			System.out.println(":: Addition ::");
			ScannerOperations.addition(first_number, second_number);
			break;
		case 4:
			System.out.println(":: Subtraction ::");
			ScannerOperations.subtraction(first_number, second_number);
			break;
		case 5:
			System.out.println(":: Modulus ::");
			ScannerOperations.modulus(first_number, second_number);
			break;
		case 6:
			System.out.println(":: Pow ::");
			ScannerOperations.pow(first_number, second_number);
			break;
		case 7:
			System.out.println(":: Square Root ::");
			ScannerOperations.squareRoot(first_number, second_number);
			break;
		default:
			System.out.println("It's not a valid option, motherfucker!");
			break;
		}
	}

}
