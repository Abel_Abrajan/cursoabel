package com.disney.projects;

import java.util.Scanner;

public class MenuOperationsWith_If {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println(":::: OPERATIONS ::::\n");
		
		System.out.println("- - - |Select an Option| - - -");
		System.out.println("1. Multiplication \n2. Division \n3. Addition \n4. Subtraction \n5. Modulus \n6. Pow \n7. Square Root");
		
		System.out.print("\nChoose an option: ");
		int option = scanner.nextInt();
		
		if(option <= 7 && option >= 1) {
			System.out.println("\nFirst number: ");
			int first_number = scanner.nextInt();
			
			System.out.println("Second number: ");
			int second_number = scanner.nextInt();
			
			if (option == 1) {
				System.out.println(":: Multiplication ::");
				ScannerOperations.multiplication(first_number, second_number);
			}
			
			if (option == 2) {
				System.out.println(":: Division ::");
				ScannerOperations.division(first_number, second_number);
			}
			
			if (option == 3) {
				System.out.println(":: Addition ::");
				ScannerOperations.addition(first_number, second_number);
			}
			
			if (option == 4) {
				System.out.println(":: Subtraction ::");
				ScannerOperations.subtraction(first_number, second_number);
			}
			
			if (option == 5) {
				System.out.println(":: Modulus ::");
				ScannerOperations.modulus(first_number, second_number);
			}
			
			if (option == 6) {
				System.out.println(":: Pow ::");
				ScannerOperations.pow(first_number, second_number);
			}
			
			if (option == 7) {
				System.out.println(":: Square Root ::");
				ScannerOperations.squareRoot(first_number, second_number);
			}
		}
		
		else {
			System.out.println("It's not a valid option, motherfucker!");
		}
	}

}
