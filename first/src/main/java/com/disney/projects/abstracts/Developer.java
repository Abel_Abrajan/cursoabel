package com.disney.projects.abstracts;

public class Developer extends Employee {
	
	private static final String WORKSTATION = "Developer";
	
	public Developer(String name) {
		super(name, WORKSTATION);	
	}
	
	@Override
	public void calculatePayment() {
		System.out.println("My rate is $400,000 to cut your grass!");
	}
}
