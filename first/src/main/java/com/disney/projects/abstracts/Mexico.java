package com.disney.projects.abstracts;

public class Mexico extends Country {
	
	public Mexico(String name) {
		super("Mexico", "Spanish");	
	}
	
	@Override
	public void calculateInhabitants() {
		System.out.println("The population in Mexico is 123.5 million people");
	}
}
