package com.disney.projects.abstracts;

import java.util.ArrayList;
import java.util.List;

public class Invoke {

	public static void main(String[] args) {
		Gardener gardener = new Gardener("Fulanito");
		// gardener.paySalary();
		
		Developer developer = new Developer("Linus");
		// developer.paySalary();
		
		Fireman fireman = new Fireman("Jack");
		
		System.out.println("Today is payment day");
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(gardener);
		employees.add(developer);
		employees.add(fireman);
		
		for (Employee employee : employees) {
			employee.paySalary();
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - -\n");
		}
	}
}
