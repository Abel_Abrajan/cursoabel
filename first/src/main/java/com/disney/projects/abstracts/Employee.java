package com.disney.projects.abstracts;

public abstract class Employee {
	
	private String name;
	private String workstation;
	
	public Employee() {
		
	}
	
	public Employee(String name, String workstation) {
		super();
		this.name = name;
		this.workstation = workstation;
	}
	
	public void paySalary() {
		System.out.println("Hi! " + getName());
		System.out.println("Your workstation is: " + getWorkstation());
		System.out.println("This is your payment ");
		calculatePayment();
	}

	public abstract void calculatePayment();
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkstation() {
		return workstation;
	}

	public void setWorkstation(String workstation) {
		this.workstation = workstation;
	}
}
