package com.disney.projects.abstracts;

public class Belice extends Country{
	
	public Belice(String name) {
		super("Belice", "Spanish");	
	}
	
	@Override
	public void calculateInhabitants() {
		System.out.println("The population in Belice is 374,681 people");
	}
}
