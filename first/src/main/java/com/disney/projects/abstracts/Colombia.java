package com.disney.projects.abstracts;

public class Colombia extends Country{
	
	public Colombia(String name) {
		super("Colombia", "Spanish");	
	}
	
	@Override
	public void calculateInhabitants() {
		System.out.println("The population in Colombia is 45.65 million people");
	}
}
