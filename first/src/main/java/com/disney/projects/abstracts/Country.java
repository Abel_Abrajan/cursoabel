package com.disney.projects.abstracts;

public abstract class Country {

	private String name;
	private String language;
	
	public Country() {
		
	}
	
	public Country(String name, String languaje) {
		super();
		this.name = name;
		this.language = languaje;
	}
	
	public void infoCountry() {
		System.out.println("The Country is: " + getName());
		System.out.println("The language is: " + getLanguage());
		System.out.println("The number of its inhabitants are: ");
		calculateInhabitants();
	}
	
	public abstract void calculateInhabitants();
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}