package com.disney.projects.abstracts;

public class Gardener extends Employee {
	
	private static final String WORKSTATION = "Gardener";
	
	public Gardener(String name) {
		super(name, WORKSTATION);	
	}
	
	@Override
	public void calculatePayment() {
		System.out.println("My rate is $20,000 to build your system!");
	}
}
