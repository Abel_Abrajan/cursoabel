package com.disney.projects.abstracts;

public class Fireman extends Employee {
	
	private static final String WORKSTATION = "Fireman";
	
	public Fireman(String name) {
		super(name, WORKSTATION);	
	}
	
	@Override
	public void calculatePayment() {
		System.out.println("My rate is $22,000 to save your life!");
	}
}
