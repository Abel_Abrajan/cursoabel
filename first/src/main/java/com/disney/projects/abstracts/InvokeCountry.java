package com.disney.projects.abstracts;

import java.util.ArrayList;
import java.util.List;

public class InvokeCountry {
	
	public static void main(String[] args) {
		Mexico mexico = new Mexico("Mexico");
		Colombia colombia = new Colombia("Colombia");
		Guatemala guatemala = new Guatemala("Guatemala");
		Belice belice = new Belice("Belice");
		
		List<Country> countries = new ArrayList<Country>();
		countries.add(mexico);
		countries.add(colombia);
		countries.add(guatemala);
		countries.add(belice);
		
		for (Country country : countries) {
			country.infoCountry();
			System.out.println("- - - - - - - - - - - - - - - - - - - - - - - -\n");
		}
	}
}
