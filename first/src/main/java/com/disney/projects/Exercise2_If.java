package com.disney.projects;

import java.util.Calendar;
import java.util.Scanner;

public class Exercise2_If {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Calendar date = Calendar.getInstance();
		
		int currentYear = date.get(Calendar.YEAR);
		
		System.out.println(":::: YEAR COMPARATOR ::::\n");
		
		System.out.println("What year are we in?");
		int inputYear = scanner.nextInt();
		
		if (inputYear >= 1) {
			
			if(inputYear == currentYear) {
				System.out.println("They are the same year!");
			}
			
			if (inputYear > currentYear) {
				int operation = inputYear - currentYear;	
				System.out.println("To reach the year " + inputYear + ", " + operation + " years remain.");
			}
			
			if(inputYear < currentYear) {
				int operation = currentYear - inputYear;
				System.out.println("Since " + inputYear + ", " + operation + " years have passed.");
			}
			
		} else {
			System.out.println("There are no negative or neutral years");
		}
	}

}
