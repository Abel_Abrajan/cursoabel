package com.disney.hierachy;

import java.util.ArrayList;
import java.util.List;

import com.disney.projects.loops.beans.Father;
import com.disney.projects.loops.beans.FirstSon;
import com.disney.projects.loops.beans.SecondSon;

public class HierachyInvokerBakers {

	public static void main(String[] args) {
		Father father = new Father("Jack", 85);
		
		System.out.println("The age of the baker is: " + father.getAge());
		father.makeBread();
		
		FirstSon firstSon = new FirstSon("Jhony", 45);
		firstSon.makeBread();
		
		SecondSon secondSon = new SecondSon("Paul", 20);
		secondSon.makeBread();
		
		System.out.println("\n<------------------------------>\n");
		
		System.out.println(father);
		System.out.println(firstSon);
		System.out.println(secondSon);
		
		List<Father> parents = new ArrayList<Father>();
		parents.add(father);
		parents.add(firstSon);
		parents.add(secondSon);
		
		System.out.println(parents);
	}
}
