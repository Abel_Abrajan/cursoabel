package com.disney.hierachy;

import com.disney.projects.loops.beans.Square;
import com.disney.projects.loops.beans.Triangle;

public class HierachyInvoker {

	public static void main(String[] args) {
		Square square = new Square(7);

        square.calculateArea();
        square.calculatePerimeter();

        System.out.println("Area= " + square.getArea());
        System.out.println("Perimeter= " + square.getPerimeter());

        Triangle triangle = new Triangle(2, 12);

        triangle.calculateArea();
        triangle.calculatePerimeter();

        System.out.println("Area: " + triangle.getArea());
        System.out.println("Perimeter: " + triangle.getPerimeter());
	}

}
