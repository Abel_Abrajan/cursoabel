package com.disney.hierachy;

import java.util.ArrayList;
import java.util.List;

public class HierachyArrays {

	public static void main(String[] args) {
		int [] exampleArray = {33, 98, 54, 10, 7, 24, 1005};
	       
        for (int i = 0; i < exampleArray.length; i++) {
            if (exampleArray[i] == 7) {
                System.out.println("I found you!");
            } else {
                System.out.println("Error 404! :(");
            }
        }
        
        List<Integer> agesList = new ArrayList<Integer>();
        
        agesList.add(55);
        agesList.add(22);
        agesList.add(28);
        agesList.add(30);
        agesList.add(19);
        agesList.add(8);
        
        System.out.println("Total people: " + agesList.size());
        
        
        if (agesList.contains(8)) {
            System.out.println("Child found");
        } else {
            System.out.println("... All good");
        }
        
        for (Integer value : agesList) {
            System.out.println("Value: " + value);
        }
	}

}
