package com.disney.hierachy;

import java.util.ArrayList;
import java.util.List;

import com.disney.projects.loops.beans.ConventionalCar;
import com.disney.projects.loops.beans.SportsCar;
import com.disney.projects.loops.beans.Vehicle;

public class HierachyInvokerCars {

	public static void main(String[] args) {
		
		SportsCar lamborghini = new SportsCar("Lamborghini", "Gallardo", "White", "Sports", 2018);
		lamborghini.makeCar();
		System.out.println("Data 1: " + lamborghini);
		
		System.out.println("\n<-------------------->\n");
		
		SportsCar lamborghini2 = new SportsCar("Lamborghini", "Aventador", "Black", "Sports", 2018);
		lamborghini.makeCar();
		System.out.println("Data 2: " + lamborghini2);
		
		System.out.println("\n<-------------------->\n");
		
		SportsCar ferrari = new SportsCar("Ferrari", "488 GTB", "Red", "Sports Coupe", 2018);
		ferrari.makeCar();
		System.out.println("Data 3: " + ferrari);
		
		System.out.println("\n<-------------------->\n");
		
		ConventionalCar fiesta = new ConventionalCar("Ford", "Fiesta", "White", "Conventional", 2018);
		fiesta.makeCar();
		System.out.println("Data 4: " + fiesta);
		
		System.out.println("\n<-------------------->\n");
		
		ConventionalCar focus = new ConventionalCar("Ford", "Focus", "Blue", "Conventional", 2018);
		focus.makeCar();
		System.out.println("Data 5: " + focus);
		
		
		System.out.println("\n<--------- CARS LIST --------->\n");
		List<Vehicle> cars = new ArrayList<Vehicle>();
		cars.add(lamborghini);
		cars.add(lamborghini2);
		cars.add(ferrari);
		cars.add(fiesta);
		cars.add(focus);
		
		System.out.println(cars);
	}
}
